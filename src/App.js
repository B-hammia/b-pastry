import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import CardsDetails from './components/CardsDetails';
import Cards from './components/Cards';
import Contact from './components/Contact';
import {Routes,Route} from "react-router-dom";
import Homme from './components/Homme';
import { ContactMail } from '@mui/icons-material';

function App() {
  return (
  <>
   <Header />
   <Routes>
   <Route path='/' element={<Homme />} exact />
     <Route path='/products' element={<Cards />} />
     <Route path='/contact' element={<Contact />} />
     <Route path='/cart/:id' element={<CardsDetails />} />
   </Routes>
  </>
  );
}

export default App;
